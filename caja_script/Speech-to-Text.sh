#!/bin/bash

notify() {
    #if [ $MINIMUM_SIZE_TO_NOTIFY -le $(stat --printf="%s" "$CHEMIN") ]; then
    notify-send -i $1 -t $NOTIFY_DURATION "$2" "$3"
    #fi
}


#Récupère le chemin du fichier en le demandant, ou en prenant la sélection caja
if [ $# -eq 0 ]; then
    CHEMIN=$(zenity --file-selection \
    --title="Veuillez selectionner le fichier à écraser" \
    --text="Choissisez le fichier");
    if [ "$CHEMIN" = "" ]; then exit 1; fi
else
    CHEMIN=$(echo -n "$CAJA_SCRIPT_SELECTED_FILE_PATHS")
fi


#Demande le modèle à utiliser
MODEL_SIZE=$(zenity --height 255 --width 340 --text "Choix du modèle Whisper :" \
    --list --radiolist --column "selection" --column "Modèle disponible" \
    FALSE "tiny" \
    FALSE "base" \
    FALSE "small" \
    FALSE "medium" \
    FALSE "large")
if [ $? -ne 0 ]; then
    #notify-send -i info -t 4700 "Annulation de la conversion."
    exit 1
elif [ $MODEL_SIZE == "" ]; then
    notify "info" "Erreur utilisateur" "Aucun format de sortie sélectionné : arrêt"
    exit 0
fi


#Execute le script python speech to text
PATH="$PATH:$HOME/.local/bin"
faster-whisper-cli "$CHEMIN" "$MODEL_SIZE" | zenity --progress \
    --height 175 --width 500 \
    --title="Transcription en cours avec Whisper $MODEL_SIZE" \
    --text="Estimation du temps en cours..." \
    --percentage=0 \
    --auto-close \
    --auto-kill \

if [ $? -ne 0 ]; then
    notify "info" "Transcription échouée ;(" "Une erreur est survenue lors de la transcription de \"$(basename $CHEMIN)\"."
else
    notify "info" "Transcription terminée !" "Le fichier \"$(basename $CHEMIN)\" a été transcrit avec succès."
fi

#!/usr/bin/env python3
#! -*- coding : utf-8 -*-

import setuptools

setuptools.setup(
    name='faster-whisper-cli',
    version='0.1.0',
    description='Une interface en ligne de commande pour Whisper',
	license="WTFPL",
    url='https://gitlab.com/breizhux/faster-whisper-cli',
    author='Breizhux',
    author_email='xavier.lanne@gmx.fr',
    packages=setuptools.find_packages(),
    install_requires=[
        'faster_whisper',
    ],
	python_requires=">=3.5",
    entry_points={
        'console_scripts': [
            'faster-whisper-cli = faster_whisper_cli.faster_whisper_cli:run_cli',
        ],
    },
)

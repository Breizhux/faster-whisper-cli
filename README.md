# faster-whisper-cli

A simple and very easy to use faster-whisper cli.


## Installation

### Installation from pip :
```
pip install git+https://gitlab.com/Breizhux/faster-whisper-cli.git
```

### In right-click of caja

You can make faster-whisper available in your right-click caja (file browser) using the script in `caja_script`.
To do this :
```bash
#1. Install faster-whisper-cli in user environment
pip install git+https://gitlab.com/Breizhux/faster-whisper-cli.git --break-system-packages

#2. Download the Speech-to-Text.sh script into ~/.config/caja/scripts
cd ~/.config/caja/scripts
wget https://gitlab.com/Breizhux/faster-whisper-cli/-/raw/main/caja_script/Speech-to-Text.sh

#3. Make it executable
chmod +x ~/.config/caja/scripts/Speech-to-Text.sh

#install zenity
sudo apt install -y zenity libnotify-bin

#Then, you can right click an audio or video file, go to scripts > Speech-to-Text.sh,
#and the magic happens, with a minimalist gui !
```

## Usage

### Mode cli :
```
faster-whisper-cli filepath.mp3 [model size (optionnal, default:medium)]
```

### In your code :
```python
import faster_whisper_cli

faster_whisper_cli.main("file.mp3", "model_size", tofile=True, tosrt=True)
```

### In Caja :
![Capture caja](https://gitlab.com/Breizhux/faster-whisper-cli/-/raw/main/ressources/captures.webp)

#!/usr/bin/env python3
#! -*- coding : utf-8 -*-

import os
import sys
import time
import subprocess
from faster_whisper import WhisperModel



DEBUG = False



class Progress :
    def __init__(self, total, eta=True, percent=True, history=3) :
        """ total   : number to be reached
            eta     : return the estimated time for arrival
            percent : return percentage of progress
            history : to calculate eta, ponderate with last past seconds."""
        self.total = total
        self.eta = eta
        self.percent = percent
        self.__start_time = time.time()
        self.__history_usage = history
        self.__history = [] #(date, current data)

    def start(self) :
        """ Restart start time (same as reset())."""
        self.__start_time = time.time()
    def reset(self) :
        """ Reset start time (same as start())."""
        self.start()

    def update(self, current, **kwargs) :
        """ Return dict with data you need."""
        self.__history.append([time.time(), current])
        data = {}
        if self.percent :
            data['percent'] = self.__get_percent(current, **kwargs)
        if self.eta :
            data['eta'] = self.__convert_s_to_human(self.__get_eta(current))
        return data

    def __get_percent(self, current, precision=2) :
        """ Return percentage of progress."""
        return round((current / self.total) * 100, precision)

    def __get_eta(self, current) :
        """ Return eta from current to self.total."""
        if len(self.__history) < 2 : return 0

        t = self.__history[-1][0]
        p = self.__get_percent(current)
        #eta from beginning
        duration = t - self.__start_time
        remaining_from_beginning = ((duration / p) * 100) - duration
        return remaining_from_beginning
        #eta from last history
        #from_time = t - self.__history_usage
        #from_data = []
        #for i in self.__history :
        #    if i[0] >= from_time :
        #        from_data = i
        #        break
        #else : from_data = self.__history[0]
        #duration = t - from_data[0]
        #remaining_from_less_past = ((duration / (p - self.__get_percent(from_data[1]))) * 100) - duration
        #average
        #eta = (remaining_from_beginning + remaining_from_less_past) / 2
        #return eta

    def __convert_s_to_human(self, t, f="%H:%M:%S") :
        """ Convert seconds in hours, minutes, seconds."""
        return time.strftime(f, time.gmtime(t))



def get_duration(file_path):
    """ Return the duration (in seconds) of video."""
    command = ['ffprobe', '-v', 'error', '-show_entries', 'format=duration', '-of', 'default=noprint_wrappers=1:nokey=1', file_path]
    if DEBUG : print("Command to get duration:", " ".join(command))
    result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return float(result.stdout)

def convert_s_to_hms(t) :
    """ Convert seconds in hours, minutes, seconds."""
    h = int(t // 3600)
    m = int((t % 3600) // 60)
    s = round(t % 60, 3)
    return h, m, s

def write_to_file(filename, seg) :
    """ Add a text segment to a text file."""
    with open(filename, 'a') as file :
        file.write(seg['text'])

def write_to_subtitle(filename, c, seg) :
    """ Add segment of subtitle in file."""
    with open(filename, 'a') as file :
        start = convert_s_to_hms(seg['start'])
        start = ":".join([str(i) for i in start]).replace(".", ",")
        end = convert_s_to_hms(seg['end'])
        end = ":".join([str(i) for i in end]).replace(".", ",")
        file.write(f"{c}\n")
        file.write(f"{start}0 --> {end}0\n")
        file.write(f"{seg['text']}\n\n")


def main(filename, model_name, tofile=True, tosrt=True, iterate=False, **kwargs) :
    """ Take a filename and model name and return subtitle list.
    Optionnal :
        - tofile : save to .txt file.
        - tosrt  : save to .srt file.
        - iterate : activate iterator of percentage progress."""
    options = {
        'compute_type' : "float32",
        #'device' : "cuda",
    }
    for i in kwargs : options[i] = kwargs[i]

    #reset output file (txt and srt) if exists :
    if tofile :
        output_file = f"{os.path.splitext(filename)[0]}.txt"
        with open(output_file, 'w') as file : pass
    if tosrt :
        output_subtitle = f"{os.path.splitext(filename)[0]}.srt"
        with open(output_subtitle, 'w') as file : pass

    #load model and file to transcribe
    model = WhisperModel(model_name, **options)
    segments, info = model.transcribe(filename, beam_size=5)
    media_duration = get_duration(filename)
    progress = Progress(media_duration)

    #transcribe audio to text
    subtitles = [] #{start:* , end:* , text:*}
    for c, segment in enumerate(segments) :
        seg = {
            "start" : round(segment.start, 3),
            "end"   : round(segment.end, 3),
            "text"  : segment.text
        }
        subtitles.append(seg)
        #print percentage of progress (compatible with zenity progress bar)
        data = progress.update(seg['end'])
        #if iterate :
        #    seg['progress'] = percent
        #    yield seg
        #else :
        sys.stdout.write(f"{int(data['percent'])}\n") ; sys.stdout.flush()
        sys.stdout.write(f"# Temps restant : {data['eta']}\n") ; sys.stdout.flush()
        if DEBUG :
            print(f"[{seg['start']} → {seg['end']}] {seg['text']}")
        #save to files
        if tofile : write_to_file(output_file, seg)
        if tosrt : write_to_subtitle(output_subtitle, c, seg)

    return subtitles


def run_cli() :
    if len(sys.argv) < 2 or sys.argv[1] in ["-h", "--help"] :
        print("usage: faster-whisper-cli filepath.mp3 [model size (optionnal)]")
        sys.exit(0)

    filename = sys.argv[1]
    if len(sys.argv) >= 3 :
        model = sys.argv[2]
    else :
        model = "medium"

    print(f"Transcribe file \"{filename}\" with model \"{model}\"")

    main(filename, model) #tofile=True, tosrt=True, iterate=False



if __name__ == "__main__" :
    run_cli()
